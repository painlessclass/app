'use strict';

describe('module: main, component: tabs-course', function () {
  var $componentController;

  beforeEach(module('main'));
  beforeEach(inject(function (_$componentController_) {
    $componentController = _$componentController_;
  }));

  it('controller exists', function () {
    // Here we are passing actual bindings to the component
    var ctrl = $componentController('tabs-course');

    expect(ctrl).toBeDefined();
  });
});
