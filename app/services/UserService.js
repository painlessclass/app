'use strict';
(function () {

  angular
    .module('main')
    .factory('UserService', UserService);

  UserService.$inject = ['Restangular'];

  function UserService (Restangular) {

    var resource = Restangular.all('users');

    var service = {
      fnSave: fnSave,
      fnGetAllCareer: fnGetAllCareer,
      fnDelete: fnDelete
    };

    return service;

    //Restful
    function fnSave (objUser) {
      return resource.post(objUser);
    }

    function fnGetAllCareer () {
      return resource.customGET();
    }

    function fnDelete (objRecord) {
      return resource.customDELETE('', {'data': objRecord});
    }

  }

})();
