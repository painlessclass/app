'use strict';
(function () {

  angular
    .module('main')
    .factory('ForumService', ForumService);

  ForumService.$inject = ['Restangular'];

  function ForumService (Restangular) {


    var resource = Restangular.all('forum');


    var service = {
      fnCreate: fnCreate,
      GetAllComment: GetAllComment,
      fnDelete: fnDelete
    };

    return service;

    //Restful
    function GetAllComment (idMatter) {
      return resource.customGET('', {'id_matter': idMatter});
    }

    function fnCreate (objElement) {
      return resource.customPOST(objElement);
    }

    function fnDelete (objRecord) {
      return resource.customDELETE('', {'data': objRecord});
    }

  }

})();
