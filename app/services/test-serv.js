'use strict';
(function () {

  angular
    .module('main')
    .factory('MatterService', MatterService);

  MatterService.$inject = ['Restangular'];

  function MatterService (Restangular) {

    var resource = Restangular.all('matter');
    var resourceMatterPublic = Restangular.all('matter/public');
    var resourceSubscriptionMatter = Restangular.all('matter/subscription');
    var service = {
      fnCreate: fnCreate,
      SubscriptionMatter: SubscriptionMatter,
      GetAllMatter: GetAllMatter,
      GetAllMatterPubluic: GetAllMatterPubluic,
      fnDelete: fnDelete
    };

    return service;

    //Restful
    function GetAllMatter () {
      return resource.customGET();
    }

    function GetAllMatterPubluic (params) {
      return resourceMatterPublic.customGET('', {'data': params});
    }

    function fnCreate (objElement) {
      return resource.customPOST(objElement);
    }

    function SubscriptionMatter (objParamas) {
      return resourceSubscriptionMatter.customPOST(objParamas);
    }

    function fnDelete (objRecord) {
      return resource.customDELETE('', {'data': objRecord});
    }

  }

})();
