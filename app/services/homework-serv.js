'use strict';
(function () {
  angular
    .module('main')
    .factory('HomeworkProvider', HomeworkProvider);

  var homeworks =
    [
      {name: 'Desarollo de Aplicación', CreateOn: ''},
      {name: 'Modelo Entidad Relación', CreateOn: ''},
      {name: 'Exposición Trabajo Final', CreateOn: ''},
      {name: 'Desarollo de Aplicación', CreateOn: ''},
      {name: 'Informe Exposición', CreateOn: ''}
    ];

  function HomeworkProvider ()
  {
    var service = {
      fnIndex: fnIndex,
      fnCreate: fnCreate
    };
    return service;
  }

  function fnIndex ()
  {
    return homeworks;
  }

  function fnCreate (newValue)
  {
    homeworks.push(newValue);
  }


})();
