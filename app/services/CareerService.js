'use strict';
(function () {

  angular
    .module('main')
    .factory('CareerService', CareerService);

  CareerService.$inject = ['Restangular'];

  function CareerService (Restangular) {


    var resource = Restangular.all('career');


    var service = {
      fnCreate: fnCreate,
      fnGetAllCareer: fnGetAllCareer,
      fnDelete: fnDelete
    };

    return service;

    //Restful
    function fnCreate (objElement) {
      return resource.customPOST(objElement);
    }

    function fnGetAllCareer () {
      return resource.customGET();
    }

    function fnDelete (objRecord) {
      return resource.customDELETE('', {'data': objRecord});
    }

  }

})();
