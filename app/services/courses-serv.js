'use strict';
(function () {

  angular
    .module('main')
    .factory('CourseProvider', CourseProvider);

  CourseProvider.$inject = ['Restangular'];

  function CourseProvider (Restangular) {

    var resource = Restangular.all('matter');

    var service = {
      fnCreate: fnCreate,
      fnGetAllMatter: fnGetAllMatter,
      fnIndex: fnIndex,
      fnShow: fnShow,
      fnUpdate: fnUpdate,
      fnDelete: fnDelete
    };

    return service;

    //Restful

    function fnCreate (objElement) {
      return resource.customPOST(objElement);
    }

    function fnGetAllMatter () {
      return resource.customGET();
    }
    function fnIndex (objParams) {
      return resource.get(objParams);
    }

    function fnShow (objParams) {
      return resource.get(objParams);
    }

    function fnUpdate (idRecord, objData) {
      return resource.one(idRecord).customPUT(objData);
    }

    function fnDelete (idRecord) {
      return resource.customDELETE(idRecord);
    }
  }

})();
