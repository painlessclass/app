'use strict';
(function () {
  angular
    .module('main')
    .factory('NoteProvider', NoteProvider);

  var notes =
    [
      {name: 'Estudiar Ionic', date: '2016-04-15', description: 'Plataforma para el desarollo de aplicaciones Móviles ver pagina http://ionicframework.com/'},
      {name: 'AWS', date: '2016-05-17', description: 'La capa gratuita de Amazon Web Services (AWS) está diseñada para permitirle obtener experiencia práctica con los servicios de la nube de AWS. La capa gratuita de AWS incluye servicios con una capa gratuita disponible durante 12 meses a partir de la fecha de inscripción en AWS, así como ofertas de servicios adicionales que no vencen al final del periodo de 12 meses de la capa gratuita de AWS.'},
      {name: 'LLamar Profesor', date: '2016-06-03', description: 'Recordar llamar a profesor para ver si hay clase'}
    ];

  function NoteProvider ()
  {
    var service =
      {
        fnIndex: fnIndex,
        fnCreate: fnCreate
      };
    return service;
  }
  function fnIndex ()
  {
    return notes;
  }
  function fnCreate (newValue)
  {
    notes.push(newValue);
  }

})();
