'use strict';
(function () {

  angular
    .module('main')
    .factory('NotesService', NotesService);

  NotesService.$inject = ['Restangular'];

  function NotesService (Restangular) {

    var resource = Restangular.all('notes');

    var service = {
      fnCreate: fnCreate,
      GetAllNotes: GetAllNotes,
      fnUpdate: fnUpdate
    };

    return service;
    //Restful
    function fnCreate (objElement) {
      return resource.customPOST(objElement);
    }

    function GetAllNotes (idMatter) {
      return resource.customGET('', {'id_matter': idMatter});
    }

    function fnUpdate (idTest) {
      return resource.customPUT(idTest);
    }

  }

})();
