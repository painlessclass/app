'use strict';
(function () {

  angular
    .module('main')
    .factory('TestService', TestService);

  TestService.$inject = ['Restangular'];

  function TestService (Restangular) {

    var resource = Restangular.all('test');

    var service = {
      fnCreate: fnCreate,
      GetAllTest: GetAllTest,
      fnUpdate: fnUpdate,
      GetAllMatterPubluic: GetAllMatterPubluic,
      fnDelete: fnDelete
    };

    return service;

    //Restful
    function fnCreate (objElement) {
      return resource.customPOST(objElement);
    }

    function GetAllTest (idMatter) {
      return resource.customGET('', {'id_matter': idMatter});
    }

    function fnUpdate (idTest) {
      return resource.customPUT(idTest);
    }

    function GetAllMatterPubluic (params) {
      return resource.customGET('', {'data': params});
    }

    function fnDelete (objRecord) {
      return resource.customDELETE('', {'data': objRecord});
    }

  }

})();
