'use strict';
(function () {

  angular
    .module('main')
    .factory('HomeworkService', HomeworkService);

  HomeworkService.$inject = ['Restangular'];

  function HomeworkService (Restangular) {

    var resource = Restangular.all('homework');

    var service = {
      fnGetAllHomework: fnGetAllHomework,
      fnCreate: fnCreate,
      fnDelete: fnDelete,
      fnUpdate: fnUpdate
    };

    return service;

    //Restful

    function fnGetAllHomework (idMatter, realizada) {
      return resource.customGET('', {'id_matter': idMatter, 'realizada': realizada});
    }

    function fnCreate (objElement) {
      return resource.customPOST(objElement);
    }

    function fnDelete (objRecord) {
      return resource.customDELETE('', {'data': objRecord});
    }

    function fnUpdate (idMatter) {
      return resource.customPUT(idMatter);
    }

  }

})();
