'use strict';
(function () {

  angular
    .module('main')
    .factory('ScheduleServices', ScheduleServices);

  ScheduleServices.$inject = ['Restangular'];

  function ScheduleServices (Restangular) {


    var resource = Restangular.all('schedule');


    var service = {
      getMatterSchedule: getMatterSchedule
    };

    return service;

    //Restful
    function getMatterSchedule () {
      return resource.customGET();
    }

  }

})();
