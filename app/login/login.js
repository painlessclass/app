'use strict';
angular.module('login', [
  'main',
  'ionic',
  'ngCordova',
  'ui.router',
  'satellizer',
  'oitozero.ngSweetAlert'
])
.config(function ($stateProvider, $urlRouterProvider, $authProvider, Config) {

  $authProvider.loginUrl = Config.ENV.SERVER_URL + 'sign_in';

  $urlRouterProvider.otherwise('/login');
  $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: 'login/templates/login.html',
      controller: 'LoginCtrl as ctrl'
    })
      .state('register', {
        url: '/register',
        templateUrl: 'login/templates/register.html',
        controller: 'RegisterCtrl as ctrl'
      });
});
