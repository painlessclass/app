'use strict';
var swal;
angular.module('login')
  .controller('LoginCtrl', function ($rootScope, $scope, $auth, $state) {
    $scope.loginForm = {
      email: 'ezesubu@gmail.com',
      password: '123123'
    };

    $scope.submitLogin = function () {
      $auth.login($scope.loginForm)
        .then(function () {
          $state.go('main.courses');
        })
        .catch(function () {
          swal('Usuario o contraseña no válida', null, 'error');
        });
    };

    $rootScope.$on('auth:login-success', function () {
      $state.go('main.courses');

    });

    $rootScope.$on('auth:login-error', function () {
      swal('Usuario o contraseña no válida', null, 'error');
    });
  });
