'use strict';
var swal;
angular.module('login')
  .controller('RegisterCtrl', function ($scope, $log, $auth, $state, UserService) {
    $scope.submitRegistration = function () {
      var promise = UserService.fnSave($scope.registrationForm);

      promise.then(function () {
        $auth.login($scope.registrationForm)
          .then(function () {
            $state.go('main.courses');
          });
      }, function (error) {
        if ( error.data.error.code === '23000' ) {
          swal('Error!', 'El usuario ya existe', 'error');
        } else {
          swal('Error!', error.data.error.message, 'error');
        }
      }
      );
    };


    $scope.$on('auth:registration-email-success', function () {
      $state.go('login');
      swal('usuario Creado!', null, 'success');
    });
    $scope.$on('auth:registration-email-error', function (v, reason) {
      swal('Error!', reason.errors.full_messages, 'error');
    });

  });
