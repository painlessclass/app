'use strict';
angular.module('main', [
  'ionic',
  'ngCordova',
  'ui.router',
  'restangular',
  'satellizer',
  'oitozero.ngSweetAlert'
  // TODO: load other modules selected during generation
])
.config(function ($stateProvider, $urlRouterProvider, $logProvider, RestangularProvider, Config) {
  $logProvider.debugEnabled(true);

  RestangularProvider.setBaseUrl(Config.ENV.SERVER_URL);
  RestangularProvider.setFullResponse(true);

  // ROUTING with ui.router
  $urlRouterProvider.otherwise('/login');
  $stateProvider
    // this state is placed in the <ion-nav-view> in the index.html
    .state('main', {
      url: '/main',
      abstract: true,
      templateUrl: 'main/templates/tabs.html'
    })
   .state('main.courses', {
     url: '/courses',
     views: {
       'tab-courses': {
         templateUrl: 'main/templates/course.html',
         controller: 'CourseShow as vm'
       }
     }
   })
    .state('main.courseDetail', {
      url: '/course/detail/:id',
      views: {
        'tab-courses': {
          templateUrl: 'main/templates/tabs-course.html'
          //  controller: 'TestController as vm'
        }
      }
    })

      .state('main.schedule', {
        url: '/schedule',
        views: {
          'tab-schedule': {
            templateUrl: 'main/components/schedule/schedule-component.html',
            controller: 'ScheduleController as vm'
          }
        }
      })
      .state('main.profile', {
        url: '/profile',
        views: {
          'tab-profile': {
            templateUrl: 'main/templates/profile.html',
            controller: 'DebugCtrl as ctrl'
          }
        }
      })
      .state('course', {
        url: '/course/:id',
        templateUrl: 'main/components/tabs-course/tabs-course-component.html',
        controller: 'TabsCourseController as vm'
      })
      .state('course.homeworks', {
        url: '/course/:id/homeworks',
        views: {
          'tab-homework': {
            templateUrl: 'main/components/homework/homework-component.html',
            controller: 'HomeworkController as vm'
          }
        }
      })
      .state('course.tests', {
        url: '/course/:id/tests',
        views: {
          'tab-prueba': {
            templateUrl: 'main/components/test/test-component.html',
            controller: 'TestController as vm'
          }
        }
      })
      .state('course.notes', {
        url: '/course/:id/notes',
        views: {
          'tab-notes': {
            templateUrl: 'main/components/notes/notes-component.html',
            controller: 'NotesController as vm'
          }
        }
      })
      .state('course.forums', {
        url: '/course/:id/forums',
        views: {
          'tab-forums': {
            templateUrl: 'main/components/forum/forum-component.html',
            controller: 'ForumController as vm'
          }
        }
      })
      .state('career', {
        url: '/career',
        templateUrl: 'main/components/career/career-list/career-list-component.html',
        controller: 'CareerList as vm'
      })
      .state('career_create', {
        url: '/career/create',
        templateUrl: 'main/components/career/career-component.html',
        controller: 'Career as vm'
      })
      .state('matter', {
        url: '/matter',
        templateUrl: 'main/components/matter/matter-component.html',
        controller: 'MatterCtrl as vm'
      });


});

