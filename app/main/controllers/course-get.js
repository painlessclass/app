'use strict';
(function () {
  angular.module('main')
    .controller('CourseGet', CourseGet);

  function CourseGet ($scope, $log, $ionicModal,  $stateParams, CourseProvider)
  {
    $log($stateParams.id);
    var vm = this;
    //vm.subjects = ['Base de Datos I','Comunicación de Datos II','Electiva Profesional 1'];
    var objParams = {
      fields: 'id,name'
    };

    var promise = CourseProvider.fnIndex(objParams);
    promise.then(
      function (objResponse) {
     //   console.log(objResponse.data.plain());
        vm.courses = objResponse.data.plain();
      },
      function (objResponse) {
        $log.debug(objResponse);
      }
    );

  }
})();
