'use strict';
(function () {

  angular.module('main')
    .controller('Test', Test);

  function Test ($scope, $ionicModal, TestProvider)
  {
    var vm = this;
    vm.tests = TestProvider.fnIndex();
    vm.testForm = {};
    $scope.submitTest = function ()
    {
      TestProvider.fnCreate(vm.testForm);
      vm.testForm = {};
      $scope.modal.hide();
    };

    $ionicModal.fromTemplateUrl('main/templates/tests/test-create.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
    });

  }
})();
