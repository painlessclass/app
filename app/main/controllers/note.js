'use strict';
(function () {

  angular.module('main')
    .controller('Note', Note);

  function Note ($scope, $ionicModal, NoteProvider)
  {
    var vm = this;
    vm.notes = NoteProvider.fnIndex();
    vm.noteForm = {};
    $scope.submitNote = function ()
    {
      NoteProvider.fnCreate(vm.noteForm);
      vm.testForm = {};
      $scope.modal.hide();
    };

    $ionicModal.fromTemplateUrl('main/templates/notes/note-create.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
    });


  }
})();
