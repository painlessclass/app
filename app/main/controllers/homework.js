'use strict';
(function () {

  angular.module('main')
    .controller('HomeWork', HomeWork);

  function HomeWork ($scope, $state, $ionicModal, HomeworkProvider)
  {
    var vm = this;

    vm.filter = {};
    vm.homeworks = HomeworkProvider.fnIndex();
    vm.homeworkForm = {};

    vm.fnBack = fnBack;

    function fnBack () {
      $state.go('main.courses');
    }

    $scope.submitHomework = function ()
    {
      vm.homeworkForm.CreateOn = Date.now();
      HomeworkProvider.fnCreate(vm.homeworkForm);
      vm.homeworkForm = {};
      $scope.modal.hide();
    };

    $ionicModal.fromTemplateUrl('main/templates/homeworks/homework-create.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
    });
  }
})();
