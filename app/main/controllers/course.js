'use strict';
(function () {

  angular.module('main')
    .controller('CourseShow', CourseShow);

  function CourseShow ($scope, $state, $log, $ionicModal, CourseProvider, CareerService, $ionicActionSheet, MatterService, $ionicPopup)
  {
    /* global moment, isEmpty, swal */
    var vm = this;
    vm.params = {
      idCareer: null,
      private: false,
      name: null,
      schedule: []
    };
    vm.careers = [];
    vm.matter = [];

    vm.fnGotoCreateCareer = fnGotoCreateCareer;
    vm.GetAllMeMatter = GetAllMeMatter;
    vm.fnGotoFindMatter = fnGotoFindMatter;
    vm.fnshowAction = fnshowAction;
    vm.fnScheduleShow = fnScheduleShow;

    fninit();

    function fninit () {
      var promise = CourseProvider.fnGetAllMatter();
      promise.then(
        function (objResponse) {
          vm.matter = objResponse.data.data;
          if (isEmpty(vm.matter))
          {
            swal('No tienes subscripciones.');
          }
        },
        function (objResponse) {
          $log.debug(objResponse);
        }
      );
      fnGetAllCareer();
    }

    function fnGetAllCareer () {

      var promise = CareerService.fnGetAllCareer();
      promise.then(
        function (objResponse) {

          vm.careers = objResponse.data.data;

        },
        function (objResponse) {
          $log.debug(objResponse);
        }
      );
    }

    function GetAllMeMatter () {

      var promise = MatterService.GetAllMatter();
      promise.then(
        function (objResponse) {

          vm.matter = objResponse.data.data;

        },
        function (objResponse) {
          $log.debug(objResponse);
        }
      );
    }

    function fnGotoCreateCareer () {
      $scope.modal.hide();
      $state.go('career_create');
    }

    function fnGotoFindMatter () {
      $state.go('matter');
    }

    $ionicModal.fromTemplateUrl('main/templates/course-create.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
      fnGetAllCareer();
    });

    $scope.submitSubject = function (objCourse) {
      if (!objCourse.id_career || !objCourse.name)
      {
        return;
      }
      if (objCourse.private)
      {
        vm.params.private = objCourse.private;
      }
      if (isEmpty(vm.params.schedule)) {
        return;
      }
      vm.params.idCareer = objCourse.id_career;
      vm.params.name = objCourse.name;

      var promise = CourseProvider.fnCreate(vm.params);

      promise.then(function () {
        $scope.modal.hide();
        fninit();
      }, function () {});
    };

    function fnshowAction () {

      $ionicActionSheet.show({
        titleText: 'Materias',
        buttons: [
          { text: '<i class="icon ion-ios-plus"></i> Agregar' },
          { text: '<i class="icon ion-ios-search"></i> Buscar' },
        ],
        cancelText: 'Cancel',
        cancel: function () {
        },
        buttonClicked: function (index) {

          if (index === 0)
          {
            $scope.modal.show();
          }
          else
          {
            fnGotoFindMatter();
          }

          return true;
        }
      });
    }

    function fnScheduleShow () {
      $scope.data = {};

      // An elaborate, custom popup
      var myPopup = $ionicPopup.show({
        template: '<label class="item item-input item-select">' +
        '<div class="input-label">' +
        'Dia' +
        '</div>' +
        '<select ng-model="data.dia">' +
        '<option value="1">LUNES</option>' +
        '<option value="2">MARTES</option>' +
        '<option value="3">MIERCOLES</option>' +
        '<option value="4">JUEVES</option>' +
        '<option value="5">VIERNES</option>' +
        '<option value="6">SABADO</option>' +
        '</select>' +
        '</label>' +
        '<span class="input-label">Hora inicio</span>' +
        '<input type="time" ng-model="data.hora_inicio" placeholder="Hora de inicio">' +
        '<span class="input-label">Hora fin</span>' +
                  '<input type="time" ng-model="data.hora_fin" placeholder="Hora de final">',
        title: 'Información de la asignatura',
        subTitle: 'Todos los campos son obligatorios.',
        scope: $scope,
        buttons: [
          { text: 'Cancel' },
          {
            text: '<b>Save</b>',
            type: 'button-positive',
            onTap: function (e) {
              if (!$scope.data.dia || !$scope.data.hora_inicio || !$scope.data.hora_fin)
              {
                e.preventDefault();
              }
              else
              {
                fnArrpushTime($scope.data);
                return;
              }
            }
          },
        ]
      });
      myPopup.then( function () {});
    }

    function fnArrpushTime (objData) {
      var jsonData = {};

      jsonData['hora_inicio'] = moment(objData.hora_inicio).format('HH-mm-ss');
      jsonData['hora_fin'] = moment(objData.hora_fin).format('HH-mm-ss');
      jsonData['dia'] = objData.dia;

      vm.params.schedule.push(jsonData);
    }

  }
})();
