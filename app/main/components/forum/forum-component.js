'use strict';
(function () {

  angular.module('main')
    .controller('ForumController', ForumController);

  function ForumController ($scope, $state, $ionicPopup, ForumService)
  {
    var vm = this;
    vm.idMatter = null;
    vm.arrComment = null;

    vm.fnAddComment = fnAddComment;

    fnInit();

    function fnInit () {
      vm.idMatter = $state.params.id;
      _fnGetAllComment();
    }

    function fnAddComment () {
      $scope.data = {};

      var myPopup = $ionicPopup.show({
        template:
        '<textarea ng-model="data.comment" placeholder="comentario..." rows="7">' +
        '</textarea>',
        title: 'Participa en el foro',
        subTitle: 'Agrega tu comentario',
        scope: $scope,
        buttons: [
          { text: 'Cancelar' },
          {
            text: '<b>Publicar</b>',
            type: 'button-positive',
            onTap: function (e) {
              if (!$scope.data.comment)
              {
                e.preventDefault();
              }
              else
              {
                _fnAddComment($scope.data.comment);
                return;
              }
            }
          },
        ]
      });
      myPopup.then( function () {});
    }

    function _fnAddComment (strComment) {
      var data = {
        description: strComment,
        idMatter: vm.idMatter
      };

      var promise = ForumService.fnCreate(data);

      promise.then(function () {
        _fnGetAllComment();
      }, function () {
      });
    }

    function _fnGetAllComment () {
      var promise = ForumService.GetAllComment(vm.idMatter);
      promise.then(function (response) {
        vm.arrComment = response.data.data;
      });
    }
  }
})();
