'use strict';
(function () {

  angular.module('main')
    .controller('TestController', TestController);

  function TestController ($scope, $state, $ionicPopup, $ionicModal, TestService,   $ionicActionSheet)
  {
    /* global moment*/
    var vm = this;
    vm.idMatter = null;
    vm.objTest = {};

    vm.params = {
      description: null,
      alarmStart: null,
      beforeTime: null,
      idMatter: null
    };

    vm.fnshowAction = fnshowAction;
    vm.fnBack = fnBack;
    vm.fnTestShowUpdate = fnTestShowUpdate;

    fnInit();
    function fnInit () {
      vm.idMatter = $state.params.id;
      _getAllTest();
    }

    $ionicModal.fromTemplateUrl('main/components/test/create/test-create.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
    });

    function fnshowAction () {

      $ionicActionSheet.show({
        cssClass: 'custom-action-sheet',
        titleText: 'Prueba',
        buttons: [
          { text: '<i class="icon ion-ios-plus"></i> Agregar' }
        ],
        cancelText: 'Cancel',
        cancel: function () {
        },
        buttonClicked: function () {
          $scope.modal.show();
          return true;
        }
      });
    }

    $scope.submitTest = function () {
      vm.params.description = vm.testForm.name;
      vm.params.alarmStart = moment(vm.testForm.date).format('YYYY-MM-DD');
      vm.params.beforeTime = vm.testForm.before_time;
      vm.params.idMatter = vm.idMatter;
      var promise = TestService.fnCreate(vm.params);

      promise.then(
        function () {
          vm.params = {};
          vm.testForm = {};
          $scope.modal.hide();
          _getAllTest();
        }, function () {}
      );
    };

    function _getAllTest () {
      var promise = TestService.GetAllTest(vm.idMatter);

      promise.then(
        function (response) {
          vm.objTest = response.data.data;
        }, function () {}
      );
    }

    function fnBack () {
      $scope.modal.hide();
    }

    function fnTestShowUpdate (idTest) {

      var myPopup = $ionicPopup.show({
        template: '',
        title: 'Cumplida',
        scope: $scope,
        buttons: [
          { text: 'Cancel' },
          {
            text: '<b>Si</b>',
            type: 'button-positive',
            onTap: function () {
              _fnShowCumplida(idTest);
            }
          },
        ]
      });
      myPopup.then(function () {
      });
    }

    function _fnShowCumplida (idTest) {
      var promise = TestService.fnUpdate(idTest);

      promise.then(
        function () {
          _getAllTest();
        }, function () {}
      );
    }


  }
})();
