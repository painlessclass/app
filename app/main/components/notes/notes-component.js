'use strict';
(function () {

  angular.module('main')
    .controller('NotesController', NotesController);

  function NotesController ($scope, $state, $ionicModal, $ionicActionSheet, NotesService, $ionicPopup)
  {
    /* global moment*/
    var vm = this;

    vm.idMater = null;
    vm.notes = {};
    vm.params = {
      name: null,
      description: null,
      idMatter: null,
      alarmStart: null,
      beforeTime: null,
      img: []
    };

    vm.fnshowAction = fnshowAction;
    vm.fnBack = fnBack;
    vm.fnUpdateNote = fnUpdateNote;
    vm.fnCamera = fnCamera;

    fnInit();
    function fnInit () {
      vm.idMater = $state.params.id;
      _fnGetAllNotes();
    }

    $ionicModal.fromTemplateUrl('main/components/notes/create/note-create.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
    });

    $scope.submitNote = function ()
    {
      vm.params.name = vm.noteForm.name;
      vm.params.description = vm.noteForm.description;
      vm.params.alarmStart = moment(vm.noteForm.alarm_start).format('YYYY-MM-DD');
      vm.params.beforeTime = vm.noteForm.before_time;
      vm.params.idMatter = vm.idMater;
      if ( vm.noteForm.img )
      {
        vm.params.img = '' + vm.noteForm.img + '' ;
      }

      var promise = NotesService.fnCreate(vm.params);
      promise.then(
        function () {
          fnBack();
          _fnGetAllNotes();
          vm.params = {};
          vm.noteForm = {};
        }, function () {}
      );
    };

    function fnshowAction () {
      $ionicActionSheet.show({
        cssClass: 'custom-action-sheet',
        titleText: 'Notas',
        buttons: [
          { text: '<i class="icon ion-ios-plus"></i> Agregar' }
        ],
        cancelText: 'Cancel',
        cancel: function () {
        },
        buttonClicked: function () {
          $scope.modal.show();
          return true;
        }
      });
    }

    function _fnGetAllNotes () {
      var promise = NotesService.GetAllNotes(vm.idMater);
      promise.then(
        function (response) {
          vm.notes = response.data.data;
        }, function () {}
      );
    }

    function fnUpdateNote (idNote) {
      var myPopup = $ionicPopup.show({
        template: '',
        title: 'Cumplida',
        scope: $scope,
        buttons: [
          { text: 'Cancel' },
          {
            text: '<b>Si</b>',
            type: 'button-positive',
            onTap: function () {
              _fnUpdate(idNote);
            }
          },
        ]
      });
      myPopup.then(function () {});
    }

    function _fnUpdate (idNote) {
      var promise = NotesService.fnUpdate(idNote);
      promise.then(
        function () {
          _fnGetAllNotes();
        }, function () {}
      );
    }

    function fnBack () {
      $scope.modal.hide();
    }

    function fnCamera () {

      /*var options = {
        quality: 50,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 100,
        targetHeight: 100,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false,
        correctOrientation:true
      };

      $cordovaCamera.getPicture(options).then(function(imageData) {
        var image = document.getElementById('myImage');
        image.src = "data:image/jpeg;base64," + imageData;
      }, function() {});*/

    }

  }
})();
