'use strict';
(function () {

  angular.module('main')
    .controller('ScheduleController', ScheduleController);

  function ScheduleController ($state, ScheduleServices)
  {
    var vm = this;
    vm.matter = [];

    vm.fnGoToMatterById = fnGoToMatterById;

    fnInit();
    function fnInit () {
      fnGetMatterSchedule();
    }

    function fnGetMatterSchedule () {
      var promise = ScheduleServices.getMatterSchedule();
      promise.then(
        function (objResponse) {
          vm.matter = objResponse.data.data;
        }, function () {});
    }

    function fnGoToMatterById (idMatter) {
      $state.go('course', {'id': idMatter});
    }
  }
})();
