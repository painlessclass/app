'use strict';
(function () {

  angular.module('main')
    .controller('MatterCtrl', MatterCtrl);

  function MatterCtrl ($scope, $state, MatterService, CareerService)
  {
    /* global isEmpty, swal, $log*/

    var vm = this;
    vm.matters = [];
    vm.idCareer = null;

    vm.fnBack = fnBack;
    vm.fnSetCareer = fnSetCareer;
    vm.fnSubscription = fnSubscription;

    fnInit();

    function fnInit () {
      fnGetAllCareer();
    }

    function fnSetCareer (idCareer) {
      vm.idCareer = idCareer;
      var params = {
        idCareer: idCareer
      };

      getMattersPublic(params);
    }

    function getMattersPublic (params) {
      var promise = MatterService.GetAllMatterPubluic(params);

      promise.then(
        function (response) {
          vm.matters = response.data.data;
          if ( isEmpty(vm.matters) )
          {
            swal('no hay materias disponibles...');
          }
        },
        function () {});
    }

    function fnBack () {
      vm.matters = [];
      vm.idCareer = null;
      $state.go('main.courses');
    }

    function fnSubscription (idMatter) {
      var params = {
        idMatter: idMatter
      };
      var promise = MatterService.SubscriptionMatter(params);

      promise.then(
        function () {
          fnSetCareer(vm.idCareer);
        }, function () {});
    }

    function fnGetAllCareer () {

      var promise = CareerService.fnGetAllCareer();
      promise.then(
        function (objResponse) {
          vm.careers = objResponse.data.data;
        },
        function (objResponse) {
          $log.debug(objResponse);
        }
      );
    }
  }
})();
