'use strict';
(function () {

  angular.module('main')
    .controller('HomeworkController', HomeworkController);

  function HomeworkController ($scope, $state, $ionicPopup, $ionicModal, $ionicActionSheet, HomeworkService)
  {
    /* global moment, isEmpty, swal*/
    var vm = this;
    var objParams = {};
    vm.idMatter = null;

    vm.homeworksOk = {};
    vm.homeworksNoOk = {};

    vm.fnshowAction = fnshowAction;
    vm.fnBack = fnBack;
    vm.fnHomeworkShowUpdate = fnHomeworkShowUpdate;

    fnInit();
    function fnInit () {
      vm.idMatter = $state.params.id;
      _fnGetAllHomeworksNoOk();
    }

    $ionicModal.fromTemplateUrl('main/components/homework/create/homework-create.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
    });

    $scope.submitHomework = function () {
      if (!vm.homeworkForm.description)
      {
        return;
      }

      objParams.idMatter = vm.idMatter;
      objParams.alarmStart = moment(vm.homeworkForm.alarm_start).format('YYYY-MM-DD');
      objParams.description = vm.homeworkForm.description;
      objParams.beforeTime = vm.homeworkForm.before_time;
      objParams.private = vm.homeworkForm.private;

      var promise = HomeworkService.fnCreate(objParams);

      promise.then(
        function () {
          vm.homeworkForm = {};
          objParams = {};

          $scope.modal.hide();
          _fnGetAllHomeworksNoOk();
        }, function () {}
      );
    };

    function fnshowAction () {

      $ionicActionSheet.show({
        cssClass: 'custom-action-sheet',
        titleText: 'Tareas',
        buttons: [
          { text: '<i class="icon ion-ios-plus"></i> Agregar' }
        ],
        cancelText: 'Cancel',
        cancel: function () {},
        buttonClicked: function () {
          $scope.modal.show();
          return true;
        }
      });
    }

    function _fnGetAllHomeworksNoOk () {
      var promise = HomeworkService.fnGetAllHomework(vm.idMatter, 1);

      promise.then(
        function (response) {
          vm.homeworksNoOk = response.data.data;

          if (isEmpty(vm.homeworksNoOk)) {
            swal('No tienes tareas pendientes para este nucleo.');
          }
        }, function () {}
      );
    }

    function fnBack () {
      $scope.modal.hide();
    }

    function _fnUpdateHomework (idMatter) {

      var promise = HomeworkService.fnUpdate(idMatter);

      promise.then(
        function () {
          _fnGetAllHomeworksNoOk();
        }, function () {}
      );
    }

    function fnHomeworkShowUpdate (idMatter) {
      $scope.data = {};

      var myPopup = $ionicPopup.show({
        template: '',
        title: 'Cumplida',
        scope: $scope,
        buttons: [
          { text: 'Cancel' },
          {
            text: '<b>Si</b>',
            type: 'button-positive',
            onTap: function () {
              _fnUpdateHomework(idMatter);
            }
          }
        ]
      });
      myPopup.then( function () {});
    }

  }
})();
