'use strict';
(function () {

  angular.module('main')
    .controller('CareerList', CareerList);

  function CareerList ($scope, $state, $log, $ionicModal, CareerService)
  {
    var vm = this;
    var objParams = {};

    vm.allCareers = null;
    vm.fnDeleteCareer = fnDeleteCareer;

    fninit();
    function fninit () {
      fnGetAllCareer();
    }
    function fnGetAllCareer () {
      var promise = CareerService.fnGetAllCareer();
      promise.then(
        function (objResponse) {
          vm.allCareers = objResponse.data.data;
        },
        function (objResponse) {
          $log.debug(objResponse);
        }
      );
    }
    function fnDeleteCareer (idCareer) {
      objParams['id_career'] = idCareer;
      var promise = CareerService.fnDelete(objParams);
      promise.then(
        function (objResponse) {
          objResponse;
          fnGetAllCareer();
        },
        function (objResponse) {
          $log.debug(objResponse);
        }
      );
    }
  }
})();
