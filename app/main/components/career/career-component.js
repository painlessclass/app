'use strict';
(function () {

  angular.module('main')
    .controller('Career', Career);

  function Career ($state, CareerService)
  {
    var vm = this;
    var objParams = {};

    vm.fnCreateCareer = fnCreateCareer;
    vm.fnBack = fnBack;

    function fnBack () {
      $state.go('main.courses');
    }
    function fnCreateCareer () {
      objParams['career'] = vm.data;

      var promise = CareerService.fnCreate(objParams);
      promise.then(
        function (objResponse) {
          if (objResponse.data.data.id)
          {
            $state.go('career');
          }
        },
        function () {}
      );

    }

  }
})();
