'use strict';
(function () {

  angular.module('main')
    .controller('TabsCourseController', TabsCourseController);

  function TabsCourseController ($state)
  {
    var vm = this;

    vm.idMatter = null;

    fnInit();
    function fnInit () {
      vm.idMatter = $state.params.id;
      $state.go('course.homeworks', {'id': vm.idMatter});
    }
  }
})();
